import Ember from 'ember';

export function relativeTime([datetime]) {
  return moment(datetime).fromNow();
}

export default Ember.Helper.helper(relativeTime);
