import Ember from 'ember';

export default Ember.Component.extend({
	tokens: [],
	//_tokens: null,
	userText: null,

	addItem(e) {
		let userText = this.get('userText');
		let tokens = this.get('tokens') || [];

		tokens.pushObject({label: userText});
		this.set('userText', null);
		//this.set("tokens", tokens);
	},

	didInsertElement() {
		let self = this;
		self.set('tokens', self.get("tokens") || []);
		self.$('input').on('keydown', (e) => {
			if (e.keyCode === 13) {
				self.addItem();
			} else if (e.keyCode === 8 || e.keyCode === 46) {
				if (!self.get('userText')) {
					self.send('deleteItem');
				}
			}
		})
	},

	actions: {
		deleteItem(item) {
			let tokens = this.get('tokens');
			if (!item && tokens && tokens.length) {
				tokens.popObject();
			} else if (item) {
				tokens.removeObject(item);
			}
			//self.set('tokens', tokens);
		}
	}
});
