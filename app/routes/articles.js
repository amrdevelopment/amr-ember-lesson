import Ember from 'ember';

export default Ember.Route.extend({
	beforeModel() {
	},
	model() {
    return Ember.$.getJSON("/api/v1/articles");
  },
  afterModel() {
  }
});
