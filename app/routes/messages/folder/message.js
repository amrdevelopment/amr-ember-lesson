import Ember from 'ember';

export default Ember.Route.extend({
	model(params) {
    let parentModel = this.modelFor('messages.folder');
    let message = parentModel.messages.findBy('threadId', params.messageId);
    return message;
  }
});
