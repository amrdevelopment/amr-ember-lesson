import Ember from 'ember';

export default Ember.Route.extend({
	model() {
		return {
			autocomplete: {
			  source: ['red','blue','green','yellow','violet','brown','purple','black','white'],
			  delay: 100
			}
		};
	}
});
