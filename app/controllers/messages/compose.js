import Ember from 'ember';

export default Ember.Controller.extend({
	recipients: [],

	displayRecipients: Ember.computed('recipients.[]', function () {
		return this.get('recipients').map((item) => item.label).join(', ');
	}),

	calculateRecipients: Ember.observer('recipients.[]', function () {
		let displayRecipients = this.get('recipients').map((item) => item.label).join(', ');
		this.set('something', displayRecipients);
	}),

	actions: {
		addAnother() {
			this.get('recipients').pushObject({label: 'test'});
		}
	}
});
