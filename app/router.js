import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('connections');
  this.route('messages', function() {
    this.route('folder', {
      path: ':folderId'
    }, function() {
      this.route('message', {
        path: ':messageId'
      });

      this.route('index', {
        path: '/'
      });
    });
    this.route('compose');
  });
  this.route('companies');
  this.route('articles', function() {
    this.route('article', {
      path: ':articleId'
    });
  });
  this.route('settings', function() {
    this.route('user');
  });
});

export default Router;
