import { relativeTime } from '../../../helpers/relative-time';
import { module, test } from 'qunit';

module('Unit | Helper | relative time');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = relativeTime(42);
  assert.ok(result);
});
